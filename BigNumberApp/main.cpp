#include "BigNumber.h"
#include <QDebug>
#include <QTextStream>
#include <iostream>

static QTextStream inputStream(stdin);

int main(int argc, char* argv[])
{
	std::cout << "Input first big number: ";
	QString bn1String = inputStream.readLine();
	std::cout << "Input second big number: ";
	QString bn2String = inputStream.readLine();

	BigNumber bn1(bn1String);
	BigNumber bn2(bn2String);

	BigNumber bn3 = bn1 + bn2;
	std::cout << "Sum is: " << bn3.toString().toStdString() << std::endl;
	inputStream.readLine();
	return 0;
}
