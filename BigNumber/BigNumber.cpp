#include "BigNumber.h"
const int base = 1e9;

BigNumber::BigNumber() noexcept
{
}

BigNumber::BigNumber(const QString& number) noexcept
{
	std::string data = number.toStdString();
	_valid = InnerToNumber(data);
}

BigNumber::BigNumber(const BigNumber& other) noexcept
{
	this->_data = other._data;
	this->_valid = other._valid;
}

bool BigNumber::operator==(const BigNumber& number) noexcept
{
	return number.Valid() == this->Valid() && this->data() == number.data();
}

BigNumber BigNumber::operator=(const BigNumber& number) noexcept
{
	this->_data = number._data;
	return *this;
}

BigNumber BigNumber::operator+(const BigNumber& number) noexcept
{
	BigNumber bn;
	if (number.Valid())
	{
		int carry = 0;
		auto numberSize = number.size();
		auto dataSize = size();
		for (int i = 0; i <= qMax(dataSize, numberSize) - 1; ++i)
		{
			if (i < dataSize)
			{
				carry += _data[i];
			}
			if (i < numberSize)
			{
				carry += number.data()[i];
			}
			bn._data.push_back(carry % base);
			carry /= base;
		}
		if (carry)
		{
			bn._data.push_back(carry);
		}
	}
	else
	{
		bn = *this;
	}

	return bn;
}

QString BigNumber::toString() const noexcept
{
	QString number;
	for (auto item : _data)
	{
		number += QString::number(item);
	}
	return number;
}

size_t BigNumber::size() const noexcept
{
	return _data.size();
}

QVector<int> BigNumber::data() const noexcept
{
	return _data;
}

bool BigNumber::InnerToNumber(std::string& number) noexcept
{
	if (number[0] == '-')
	{
		return false;
	}

	if (number.size() == 0)
	{
		_data.push_back(0);
		return true;
	}

	while (number.size() % 9 != 0)
	{
		number = '0' + number;
	}

	auto numSize = number.size();
	_data.reserve(numSize);
	for (int i = 0; i < numSize; i += 9)
	{
		int v = 0;
		for (int j = i; j < i + 9; ++j)
		{
			v = v * 10 + (number[j] - '0');
		}
		_data.insert(_data.begin(), v);
	}
	return true;
}
