#pragma once

#include <QVector>

class BigNumber
{
public:
	BigNumber() noexcept;
	BigNumber(const QString& string) noexcept;
	BigNumber(const BigNumber& other) noexcept;

	bool operator ==(const BigNumber& number) noexcept;
	BigNumber operator =(const BigNumber& number) noexcept;
	BigNumber operator +(const BigNumber& number) noexcept;

	QString toString() const noexcept;
	size_t size() const noexcept;
	QVector<int> data() const noexcept;
	bool Valid() const noexcept { return _valid; }
protected:
	bool _valid = false;
	QVector<int> _data;
	bool InnerToNumber(std::string& number) noexcept;

};
