#include "TestBigNumber.h"
#include "BigNumber.h"

TestBigNumber::TestBigNumber(QObject* parent)
	: QObject(parent)
{
}

void TestBigNumber::CreateValidBigNumber()
{
	BigNumber bn("1234567890");
	QCOMPARE(bn.Valid(), true);
}

void TestBigNumber::CreatInvalidBigNumber()
{
	BigNumber bn("-1234567890");
	QCOMPARE(bn.Valid(), false);
}

void TestBigNumber::CreateEmptyBigNumber()
{
	BigNumber bn;
	QCOMPARE(bn.Valid(), false);
}

void TestBigNumber::SumValidAndInvalid()
{
	BigNumber invalidBn;
	BigNumber validBN("123456789");
	BigNumber result = validBN + invalidBn;
	QCOMPARE(validBN.data(), result.data());
}

void TestBigNumber::ValidSum()
{
	QString result = "24691356";
	BigNumber bn1("12345678");
	BigNumber sum = bn1 + bn1;
	QCOMPARE(sum.toString(), result);
}
