#pragma once

#include <QtTest/QtTest>

class TestBigNumber : public QObject
{
	Q_OBJECT
public:
	explicit TestBigNumber(QObject* parent = Q_NULLPTR);
private Q_SLOTS:
	void CreateValidBigNumber();
	void CreatInvalidBigNumber();
	void CreateEmptyBigNumber();
	void SumValidAndInvalid();
	void ValidSum();
};
