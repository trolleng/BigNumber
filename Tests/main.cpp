#include <qtestcase.h>
#include "TestBigNumber.h"

int main(int argc, char* argv[])
{
	QCoreApplication application(argc, argv);
	QTest::qExec(new TestBigNumber(), argc, argv);
	return application.exec();
}
